/*
 * Macro template to process multiple images in a folder
 */

#@ File (label = "Input directory", style = "directory") input
#@ File (label = "Output directory", style = "directory") output
#@ String (label = "File suffix", value = ".tif") suffix

// See also Process_Folder.py for a version of this code
// in the Python scripting language.

stardist_processFolder(input, output);

// function to scan folders/subfolders/files to find files with correct suffix
//function

// from de.csbdresden.stardist import StarDist2D 


function stardist_processFolder (input, output) {
	//args=getArgument();
	//input=substring(args, (indexOf(args,"input")+6), indexOf(args,"output")-1);
	//print(input);
	//output=substring(args, (indexOf(args,"output")+7), lengthOf(args));
	//print(output);

	suffix = ".tif";
	list = getFileList(input);
	list = Array.sort(list);
	for (i = 0; i < list.length; i++) {
		// if(File.isDirectory(input + File.separator + list[i]))
		//	processFolder(input + File.separator + list[i]);
		// if(endsWith(list[i], suffix))
		if(endsWith(list[i], suffix))
			processFile(input, output, list[i]);
	}
}

function processFile(input, output, file) {
	// Do the processing here by adding your own code.
	open(input + File.separator + file);
	run("Command From Macro", "command=[de.csbdresden.stardist.StarDist2D], args=['input':"+ file+", 'modelChoice':'Versatile (fluorescent nuclei)', 'normalizeInput':'true', 'percentileBottom':'0.0', 'percentileTop':'98.2', 'probThresh':'0.479071', 'nmsThresh':'0.3', 'outputType':'Both', 'nTiles':'1', 'excludeBoundary':'2', 'roiPosition':'Automatic', 'verbose':'false', 'showCsbdeepProgress':'true', 'showProbAndDist':'true'], process=[false]");
	//run( "command=[de.csbdresden.stardist.StarDist2D]", "args=['input':"+ file+", 'modelChoice':'Versatile (fluorescent nuclei)', 'normalizeInput':'true', 'percentileBottom':'0.0', 'percentileTop':'98.2', 'probThresh':'0.479071', 'nmsThresh':'0.3', 'outputType':'Both', 'nTiles':'1', 'excludeBoundary':'2', 'roiPosition':'Automatic', 'verbose':'false', 'showCsbdeepProgress':'true', 'showProbAndDist':'true'], process=[false]");
	selectWindow("Label Image");
	saveAs("Tiff", output + File.separator + list[i]);
	run("Close All");
	// Leave the print statements until things work, then remove them.
	print("Processing: " + input + File.separator + file);
	print("Saving to: " + output);
}
