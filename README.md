# RNAScopeAnalysis

The sript *fRNAscope2dMan.mlx* analyses RNA scope images.
The images have four channels:

    -   Channels 1,2,4 - molecules to be counted
    -   Channel 3 DAPI - nuclei the molecules are assigned to

1. The parameters are calibrated on a test image: thresholds for filtered channels 1,2,4 and minimal nucleus size (in pixels)
2. Nuclei are segmented with a custom written script (stardist = 0) or the Fiji Stardist plugin (stardist = 1)
3. Nuclei labels can be manually corrected using Matlab's *imageLabeler*
4. Batch analysis is performed: molecules inside each (dilated) nucleus are counted (using the parameters established at step 1) and saved in the tables in the result folder  

![plot](./Res.png)

**Reference:**

Velazquez-Sanchez C, Muresan L, Marti-Prats L, Belin D. The development of compulsive coping behaviour is associated with a downregulation of Arc in a Locus Coeruleus neuronal ensemble. Neuropsychopharmacology. 2023 Jan 12. doi: 10.1038/s41386-022-01522-y. 
