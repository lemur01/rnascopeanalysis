
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% lam94@cam.ac.uk, 2020
% Batch analysis in 2d
%
% fullAnalysis  = 0; % only segment the DAPI
%               > 0; % segment the DAPI, analyse fluorescence signal and
%               save the results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function makeDAPI(pt, respt)

if ~exist('pt')
    pt = uigetdir('','Select folder with images');  % folder where tif images are stored
end
if ~exist('respt')
    respt = uigetdir('','Select result folder');    % folder where result images are stored
end


files = dir(fullfile(pt, '*.tif'));
%mkdir(fullfile(respt, 'Labels'))
mkdir(fullfile(respt, 'DAPI'))

T = sort({files.name});
imageFilenames = cellfun(@(x) fullfile(pt,x), T, 'UniformOutput', false); %fullfile(pt, files.name);
for k = 1:length(T)
   
    disp(strcat(num2str(k), '/',num2str(length(T))))
    T{k}
    img = Read3d(fullfile(pt,T{k}));
    imwrite(uint8((img(:,:,3))/max(max(img(:,:,3)))*255),fullfile(respt, 'DAPI', strcat(strtok(T{k}, '.'), '.tif')), 'tif');
  
end


