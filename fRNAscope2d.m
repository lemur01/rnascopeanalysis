
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% lam94@cam.ac.uk, 2020
% analysis of RNAscope images (2d)
% Input: 
% pt    - input file's folder
% fn    - input file's name
% respt - folder with results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function fRNAscope2d(pt, fn, respt, t1, th2, th4, thA, gTruth)

if ~exist('fullAnalysis')
    fullAnalysis = 1;
end

if ~exist('gTruth')
    gTruth = [];
end
fname = fullfile(pt,fn);
%% Read data
original = Read3d(fname);
img = double(original);

removespot = 19;
enlarge_param = 10;
% small_regmax = 5;
q = 0.01;
% tcenters = [];

    index = find(contains(gTruth.DataSource.Source, fn));
    mask = imread(gTruth.LabelData.PixelLabelData{index});
    L2 = bwlabel(mask);
    BW = bwperim(L2>0,8);
    %% Detect blobs
    for k = 1:4        
        [Denoised(:,:,k) Res(:,:,k) Backgr(:,:,k) noiseStd wave{k}] = FindPeakWav(medfilt2(double(img(:,:,k))), q,0, 3,1,-1);
        Res(:,:,k)= bwareaopen(Res(:,:,k),removespot);
    end
    %% Channel 1 - detection
    %figure; imagesc(img(:,:,1)); title('Original channel 1'); hold on
    im1 = imgaussfilt(img(:,:,1), 0.5)-imgaussfilt(img(:,:,2), 3);
    im1 = im1/(max(im1(:)))*100;
    if ~exist('th1')
        th1 = median(im1(:))+3*std(im1(:));
    end

    max1 = imregionalmax(im1.*(im1>th1),8);
    [det1x, det1y] = find(max1>0);

    %% Channel 2 - detection
    %figure; imagesc(img(:,:,2)); title('Original channel 2'); hold on
    im2 = imgaussfilt(img(:,:,2), 0.5)-imgaussfilt(img(:,:,2), 3);
    im2 = im2/(max(im2(:)))*100;

    if ~exist('th2')
        th1 = median(im2(:))+3*std(im2(:));
    end

    max2 = imregionalmax(im2.*(im2>th2),8);
    [det2x, det2y] = find(max2>0);
%     figure; imagesc(im2); title('Filtered channel 2'); hold on
%     axis equal tight
%     caxis([0 100])
%     hold on

%     plot(det2y, det2x, 'mo');hold on
    %% Channel 4 - detection
    %figure; imagesc(img(:,:,2)); title('Original channel 2'); hold on
    im4 = imgaussfilt(img(:,:,4), 0.5)-imgaussfilt(img(:,:,4), 3);
    im4 = im4/(max(im4(:)))*100;

    if ~exist('th2')
        th1 = median(im4(:))+3*std(im4(:));
    end

    max4 = imregionalmax(im4.*(im4>th4),8);
    [det4x, det4y] = find(max4>0);
%     figure; imagesc(im2); title('Filtered channel 2'); hold on
%     axis equal tight
%     caxis([0 100])
%     hold on

%     plot(det2y, det2x, 'mo');hold on

    %% Visualise result
    xx1 = [det1y, det1x];
    xx2 = [det2y, det2x];
    xx4 = [det4y, det4x];
    clf;
    or(:,:,1) = original(:,:,1)/max(max(original(:,:,1)));
    or(:,:,2) = original(:,:,2)/max(max(original(:,:,2)));
    or(:,:,3) = original(:,:,3)/max(max(original(:,:,3)));
    imagesc(or); hold on
    plot(xx1(:,1), xx1(:,2), 'm.');hold on
    plot(xx2(:,1), xx2(:,2), 'c.');
    plot(xx4(:,1), xx4(:,2), 'y.');
    [x y] = find(BW>0);
    plot( y, x, 'w.');
    axis equal tight
    saveas(gcf , strcat(respt, strtok(fn, '.'), 'Detection', '.png'));
    saveas(gcf, strcat(respt, strtok(fn, '.'), 'Detection', '.fig'));
    
    %% Save results

    intsig1 = regionprops(L2, Denoised(:,:,1)-Backgr(:,:,1), 'MeanIntensity','Area');
    intsig2 = regionprops(L2, Denoised(:,:,2)-Backgr(:,:,2), 'MeanIntensity','Area');
    intsig4 = regionprops(L2, Denoised(:,:,4)-Backgr(:,:,4), 'MeanIntensity','Area');
    r1 = L2(sub2ind(size(L2),floor(xx1(:,2)), floor(xx1(:,1))));
    r2 = L2(sub2ind(size(L2),floor(xx2(:,2)), floor(xx2(:,1))));
    r4 = L2(sub2ind(size(L2),floor(xx4(:,2)), floor(xx4(:,1))));
    [GC1,GR1]=groupcounts(r1);
    [GC2,GR2]=groupcounts(r2);
    [GC4,GR4]=groupcounts(r4);
    T1 = table(GR1,GC1);
    T1.Properties.VariableNames = {'CellId', 'No_1'};

    T2 = table(GR2,GC2);
    T2.Properties.VariableNames = {'CellId', 'No_2'};


    T4 = table(GR4,GC4);
    T4.Properties.VariableNames = {'CellId', 'No_4'};

    T1a = table([1:max(L2(:))]', [intsig1.Area]'.*[intsig1.MeanIntensity]');
    T2a = table([1:max(L2(:))]', [intsig2.Area]'.*[intsig2.MeanIntensity]');
    T1a.Properties.VariableNames = {'CellId', 'IntSignal1'};
    T2a.Properties.VariableNames = {'CellId', 'IntSignal2'};
    T4a = table([1:max(L2(:))]', [intsig4.Area]'.*[intsig4.MeanIntensity]');
    T4a.Properties.VariableNames = {'CellId', 'IntSignal4'};

    T = outerjoin(outerjoin(T1,T2,'MergeKeys',true),T4,'MergeKeys',true);
    Tb = outerjoin(outerjoin(T1a,T2a,'MergeKeys',true),T4a,'MergeKeys',true);
    T = outerjoin(T,Tb,'MergeKeys',true);
    ind = ones(size([1:max(L2(:))]'));
    %ind(L2(sub2ind(size(L2),floor(dy),floor(dx)))) = 0;
    Tdel = table([1:max(L2(:))]', ind);
    Tdel.Properties.VariableNames = {'CellId', 'Valid'};
    writetable(outerjoin(T,Tdel,'MergeKeys',true), fullfile(respt, strcat(strtok(fn, '.'),'Res.xls')));
    %imwrite(uint16(L2), fullfile(respt, strcat(strtok(fn, '.'),'Mask.tif' )))
close all
