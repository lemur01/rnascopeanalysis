
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% lam94@cam.ac.uk, 2020
% Batch analysis in 2d
%
% make DAPI folder
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function makeDAPIfolder(pt, respt, thA)

if ~exist('thA')
    thA = 100;
end 
if ~exist('pt')
    pt = uigetdir('','Select folder with images');  % folder where tif images are stored
end
if ~exist('respt')
    respt = uigetdir('','Select result folder');    % folder where result images are stored
end


files = dir(fullfile(pt, '*.tif'));
%mkdir(fullfile(respt, 'Labels'))
mkdir(fullfile(respt, 'DAPI'))
%% Parameters
% th1 =1;% - threshold for channel 1
% th2 =1;% - threshold for channel 2
% thA  = 100;- minimum area for DAPI
enlarge_param = 10;

%%
%Create a groundTruth object.
T = sort({files.name});
imageFilenames = cellfun(@(x) fullfile(pt,x), T, 'UniformOutput', false); %fullfile(pt, files.name);
%dataSource = groundTruthDataSource(imageFilenames);
%%
for k = 1:length(T)
   
    disp(strcat(num2str(k), '/',num2str(length(T))))
    T{k}
    img = Read3d(fullfile(pt,T{k}));
    imwrite(uint8((img(:,:,3))/max(max(img(:,:,3)))*255),fullfile(respt, 'DAPI', strcat(strtok(T{k}, '.'), '.tif')), 'tif');
    
end    

