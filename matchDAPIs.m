
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% lam94@cam.ac.uk, 2020
% Batch analysis in 2d
%
% only segment the DAPI
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function gTruth =matchDAPIs(pt, thA, respt)

if ~exist('thA')
    thA = 100;
end 
if ~exist('pt')
    pt = uigetdir('','Select folder with DAPI images');  % folder where tif images are stored
end
if ~exist('respt')
    respt = uigetdir('','Select results folder for Stardist');    % folder where result images are stored.    
end

files = dir(fullfile(respt, '*.tif'));
mkdir(fullfile(respt, 'BinLabels'))
% mkdir(fullfile(respt, 'DAPI'))
%% Parameters

% thA  = 100;- minimum area for DAPI
enlarge_param = 10;

fun = @(x) length(unique(x(:)))>1;

%Create a groundTruth object.
T = sort({files.name});
%T = struct2table(files)
%imageFilenames = cellfun(@(x) fullfile(pt,x), T.name, 'UniformOutput', false); %fullfile(pt, files.name);
% imageFilenames = cellfun(@(x) fullfile(pt,x), T, 'UniformOutput', false); %fullfile(pt, files.name);
% files = dir(fullfile(respt, '*.tif'));
% dataSource = groundTruthDataSource(imageFilenames);
% imageFilenames = cellfun(@(x) fullfile(respt, x), T, 'UniformOutput', false); %fullfile(pt, files.name);
% dataSource1 = groundTruthDataSource(imageFilenames);
for k = 1:length(T)
   
    disp(strcat(num2str(k), '/',num2str(length(T))))
    T{k}
    L = Read3d(fullfile(respt,T{k}));
    perim = 1-nlfilter(L,[3 3],fun);
    bw = (L>0).*perim;
    enlarge = bwdist(bw);
    L1 = watershed(enlarge);
    L2 = (enlarge<enlarge_param)
    L2 = bwlabel(L2.*(L1>0));
    % enlarge = bwdist(b{1});   
%    L2 =  bwlabel(bw);
%     enlarge = bwdist(b{1});
%     L2 = (enlarge<enlarge_param).*double(L1);
     stat =regionprops(L2, 'Area');
    %% Filter DAPI based on area 
    
    idA = find([stat.Area]>thA);
    mask = (zeros(size(L2)));
    mask(ismember(L2,unique(idA))) = 1;
    L2 = bwlabel(L2.*mask);
    %BW = bwperim(L2>0,8);
    %fullfile(respt, 'Labels' ,strcat(strtok(T{k}, '.'), 'Label.png'))
    %imwrite((L2.*perim)>0 , fullfile(respt, 'BinLabels' ,strcat(strtok(T{k}, '.'), 'Label.png')), 'png')
    imwrite(L2>0 , fullfile(respt, 'BinLabels' ,strcat(strtok(T{k}, '.'), 'Label.png')), 'png')
end
%%
imageFilenames = cellfun(@(x) fullfile(pt, x), T, 'UniformOutput', false); %fullfile(pt, files.name);
dataSource1 = groundTruthDataSource(imageFilenames);
%%
ldc =labelDefinitionCreator();
addLabel(ldc,'Nucleus',labelType.PixelLabel);
%addAttribute(ldc,'Nucleus','Color',attributeType.List,{'Green'})
%addLabel(ldc,'Background',labelType.PixelLabel);
labelDefs = create(ldc)   
%%
%dataF = dir(fullfile(respt, 'BinLabels','*.png'));   
dataF = dir(fullfile(respt, '*.png'));   
%dT = struct2table(dataF)
dT = sort({dataF.name})';
dataFile = cellfun(@(x) fullfile(respt, 'BinLabels',x), dT, 'UniformOutput', false); %fullfile(pt, files.name);
labelData = table(dataFile,'VariableNames',{'PixelLabelData'});
gTruth = groundTruth(dataSource1,labelDefs,labelData);
save(fullfile(respt, 'gStarDistTruth.mat'), 'gTruth')
