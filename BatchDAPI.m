
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% lam94@cam.ac.uk, 2020
% Batch analysis in 2d
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function gTruth = BatchDAPI(pt, respt,  th1, th2, thA)

if ~exist('thA')
    thA = 100;
end 
if ~exist('pt')
    pt = uigetdir('','Select folder with images');  % folder where tif images are stored
end
if ~exist('respt')
    respt = uigetdir('','Select result folder');    % folder where result images are stored
end


files = dir(fullfile(pt, '*.tif'));
mkdir(fullfile(respt, 'Labels'))
mkdir(fullfile(respt, 'DAPI'))
%% Parameters
% th1 =1;% - threshold for channel 1
% th2 =1;% - threshold for channel 2
% thA  = 100;- minimum area for DAPI
enlarge_param = 10;
%%
% fRNAscope2d(pt, files, respt, th1, th2, thA, fullAnalysis); 
% for k = 1:length(files)   
%     disp(strcat(num2str(k), '/',num2str(length(files))))
%     files(k).name
%     fRNAscope2d(pt, files(k).name, respt, th1, th2, thA, fullAnalysis); 
% end
%%
%Create a groundTruth object.
T = sort({files.name});
%T = struct2table(files)
%imageFilenames = cellfun(@(x) fullfile(pt,x), T.name, 'UniformOutput', false); %fullfile(pt, files.name);
imageFilenames = cellfun(@(x) fullfile(pt,x), T, 'UniformOutput', false); %fullfile(pt, files.name);
dataSource = groundTruthDataSource(imageFilenames);
%%
for k = 1:length(T)
   
    disp(strcat(num2str(k), '/',num2str(length(T))))
    T{k}
    img = Read3d(fullfile(pt,T{k}));
    %imwrite(uint8((img(:,:,3))/max(max(img(:,:,3)))*255),fullfile(respt, 'DAPI', strcat(strtok(T{k}, '.'), '.tif')), 'tif');
    
    sim=imgaussfilt(double(imgaussfilt(double(img(:,:,3)), 7)));
    thresh = multithresh(sim,3);
    seg_im = imquantize(img(:,:,3),thresh);
    % Find outlines for different inetensity nuclei
    for m = 3:-1:1
        b{m} = bwareaopen(imfill(seg_im>m, 'holes'), 100);%(4-k)^2*500);
        for it = 1:50      
            b{m} = medfilt2(b{m});
        end
        b{m} =imfill(b{m}, 'holes');
        d{m} =bwdist(~b{m});
    end
    L1=watershed(imhmin((-d{1}-d{2}-d{3}).*b{1},3));
    L =  bwlabel(double(L1).*(b{1}>0));
    enlarge = bwdist(b{1});
    L2 = bwlabel((enlarge<enlarge_param).*double(L1));
    stat =regionprops(L2, 'Area');
    L1=watershed(imhmin((-d{1}-d{2}-d{3}).*b{1},3));
    L2 =  bwlabel(double(L1).*(b{1}>0));
%     enlarge = bwdist(b{1});
%     L2 = (enlarge<enlarge_param).*double(L1);
     stat =regionprops(L2, 'Area');
    %% Filter DAPI based on area 

    idA = find([stat.Area]>thA);
    size(idA)
    mask = zeros(size(L));
    mask(ismember(L2,unique(idA))) = 1;
    L2 = bwlabel(L2.*mask);
    BW = bwperim(L2>0,8);
    fullfile(respt, 'Labels' ,strcat(strtok(T{k}, '.'), 'Label.png'))
    imwrite(L2>0 , fullfile(respt, 'Labels' ,strcat(strtok(T{k}, '.'), 'Label.png')), 'png')
end
%%
imageFilenames = cellfun(@(x) fullfile(respt, 'DAPI', x), T, 'UniformOutput', false); %fullfile(pt, files.name);
dataSource1 = groundTruthDataSource(imageFilenames);
%%
ldc =labelDefinitionCreator();
addLabel(ldc,'Nucleus',labelType.PixelLabel);
%addAttribute(ldc,'Nucleus','Color',attributeType.List,{'Green'})
%addLabel(ldc,'Background',labelType.PixelLabel);
labelDefs = create(ldc)   
%%
dataF = dir(fullfile(respt, 'Labels','*.png'));   
%dT = struct2table(dataF)
dT = sort({dataF.name})';
dataFile = cellfun(@(x) fullfile(respt, 'Labels',x), dT, 'UniformOutput', false); %fullfile(pt, files.name);
labelData = table(dataFile,'VariableNames',{'PixelLabelData'});
gTruth = groundTruth(dataSource1,labelDefs,labelData);
